const runWithFrameRate = function (fps) {
	return fn => {
		let then = Date.now()

		// custom fps, otherwise fallback to 60
		fps = fps || 60
		let interval = 1000 / fps

		let abort = false

		return (function loop(time) {
			if (!abort) requestAnimationFrame(loop)

			var now = Date.now()
			var delta = now - then

			if (delta > interval) {
				// Update time
				// now - (delta % interval) is an improvement over just
				// using then = now, which can end up lowering overall fps
				then = now - (delta % interval)

				// call the fn
				abort = fn(delta)
			}
		})(0)
	}
}

export default runWithFrameRate


const _oldRunWithFrameRate = (fps = 60) => {
	const fpsInterval = 1000 / fps
	return async func => {
		do {
			let then = Date.now()
			if (func()) return
			do {
				await vsync()
			} while (Date.now() - then < fpsInterval)
		} while (true)
	}
}

const vsync = () => new Promise(window.requestAnimationFrame)
